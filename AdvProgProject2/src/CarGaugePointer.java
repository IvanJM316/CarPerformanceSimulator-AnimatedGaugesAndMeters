import java.awt.Color;
import java.awt.Graphics;

/**
 * Objects of this class represent a generic car gauge pointer.
 * @author Ivan J. Miranda Marrero
 *
 */
public class CarGaugePointer 
{
	private int xLocation; //The x coordinate of the gauge (circle) in which the pointer will be drawn.
	private int yLocation; //The y coordinate of the gauge (circle) in which the pointer will be drawn.
	private int radius; //The radius of the gauge in which the pointer will be drawn.
	private int initialPosition; //The initial position of the pointer in the desired gauge, in degrees, in which the pointer will be drawn.
	private int centerX; //The center's x coordinate of the gauge (circle) in which the pointer will be drawn.
	private int centerY; //The center's y coordinate of the gauge (circle) in which the pointer will be drawn.
	
	/**
	 * Creates a car gauge pointer with the given properties.
	 * @param xLocation the x coordinate of the gauge (circle) in which the pointer will be drawn.
	 * @param yLocation the y coordinate of the gauge (circle) in which the pointer will be drawn.
	 * @param radius the radius of the gauge in which the pointer will be drawn.
	 * @param initialPosition the initial position of the pointer in the desired gauge, in degrees, in which the pointer will be drawn.
	 */
	public CarGaugePointer(int xLocation, int yLocation, int radius, int initialPosition)
	{
		this.xLocation = xLocation;
		this.yLocation = yLocation;
		this.radius = radius;
		this.initialPosition = initialPosition;
		centerX = xLocation + radius;
		centerY = yLocation + radius;
	}
	
	/**
	 * Draws the current instance of this car gauge pointer with the given properties.
	 * @param g the Graphics instance in which this object will be drawn.
	 * @param c the color in which it will be drawn.
	 * @param centerColor the color in which the origin of the pointer will be drawn.
	 */
	public void draw(Graphics g, Color c, Color centerColor)
	{
		Color originalColor = g.getColor(); //Saves original color in the Graphics instance.
		
		//Drawing the pointer.
		g.setColor(c);
		//Coordinates are obtained by pythagorean geometry. dx = rcos(z), dy = rsin(z) where r is the radius of the circle.
		g.drawLine(centerX, centerY, (int)(centerX + radius*Math.cos(Math.toRadians(initialPosition))), (int)(centerY + radius*Math.sin(Math.toRadians(initialPosition))));
		g.drawLine(centerX, centerY + 1, (int)(centerX + radius*Math.cos(Math.toRadians(initialPosition))), (int)(centerY + radius*Math.sin(Math.toRadians(initialPosition))));
		g.drawLine(centerX, centerY - 1, (int)(centerX + radius*Math.cos(Math.toRadians(initialPosition))), (int)(centerY + radius*Math.sin(Math.toRadians(initialPosition))));
		g.drawLine(centerX - 1, centerY, (int)(centerX + radius*Math.cos(Math.toRadians(initialPosition))), (int)(centerY + radius*Math.sin(Math.toRadians(initialPosition))));
		g.drawLine(centerX + 1, centerY, (int)(centerX + radius*Math.cos(Math.toRadians(initialPosition))), (int)(centerY + radius*Math.sin(Math.toRadians(initialPosition))));



		//Drawing the origin of the pointer.
		g.setColor(centerColor);
		g.drawArc((int)(xLocation + (15.0*radius)/16), (int)(yLocation + (15.0*radius)/16), (int)(radius/8.0), (int)(radius/8.0), 0, 360);
		g.fillArc((int)(xLocation + (15.0*radius)/16), (int)(yLocation + (15.0*radius)/16), (int)(radius/8.0), (int)(radius/8.0), 0, 360);
		
		g.setColor(originalColor); //Sets back the color to the original color the Graphics instance had before using this method.
		
	}
}

