import java.awt.Graphics;
import java.awt.Color;

/**
 * Objects of this type represent a car wheel with eight lines.
 * @author Ivan J. Miranda
 *
 */
public class CarWheel
{
	private int xLocation; //Wheel's x coordinate.
	private int yLocation; //Wheel's y coordinate.
	private int radius; //Wheel's radius
	private int centerX; //Wheel's center x coordinate.
	private int centerY; //Wheel's center y coordinate.
	private int startingAngle; //Wheel's reference angle (for rotation animation).
	
	/**
	 * Creates a car's wheel with the properties given.
	 * @param xLocation the wheel's x coordinate (top-left).
	 * @param yLocation the wheel's y coordinate (top-left).
	 * @param radius the wheel's radius.
	 * @param startingAngle the initial position of the wheel's reference point in degrees.
	 */
	public CarWheel(int xLocation, int yLocation, int radius, int startingAngle)
	{
		this.xLocation = xLocation;
		this.yLocation = yLocation;
		this.radius = radius;
		centerX = xLocation + radius;
		centerY = yLocation + radius;
		this.startingAngle = startingAngle;
	}
	/**
	 * Draws the current instance of this wheel with the given properties.
	 * @param g the Graphics instance in which this object will be drawn.
	 * @param c the color in which it will be drawn.
	 */
	public void draw(Graphics g, Color c)
	{
		Color originalColor = g.getColor(); //Saves original color in the Graphics instance.
		
		g.setColor(c);
		g.drawArc(xLocation, yLocation, radius*2, radius*2, 0, 360);
		int angleBetweenArcs = 360/8; //as this is a tire with 8 lines.
		int varyingAngle = startingAngle; //the angle that will be used to find the points to draw lines. It will depend on the starting angle.
		for (int i = 0; i < 8; i++)
		{
			//Coordinates are obtained by pythagorean geometry. dx = rcos(z), dy = rsin(z) where r is the radius of the circle.
			g.drawLine(centerX, centerY, (int)(centerX + radius*Math.cos(Math.toRadians(varyingAngle))), (int)(centerY + radius*Math.sin(Math.toRadians(varyingAngle))));
			varyingAngle += angleBetweenArcs; //The angle is then increased by 45 degrees for the next line drawing.
		}
		g.drawArc((int)(centerX + radius*Math.cos(Math.toRadians(startingAngle))) - (int)((1.0*radius)/16.0), //radius of small circle is
				  (int)(centerY + radius*Math.sin(Math.toRadians(startingAngle))) - (int)((1.0*radius)/16.0), //1/16th the radius of the wheel.
				  (int)((1.0*radius)/8.0), (int)((1.0*radius)/8.0), 0, 360									  //in order to keep resizable ratios.
				  );
		g.fillArc((int)(centerX + radius*Math.cos(Math.toRadians(startingAngle))) - (int)((1.0*radius)/16.0), 
				  (int)(centerY + radius*Math.sin(Math.toRadians(startingAngle))) - (int)((1.0*radius)/16.0), 
				  (int)((1.0*radius)/8.0), (int)((1.0*radius)/8.0), 0, 360
				  );
		
		g.setColor(originalColor); //Sets back the color to the original color the Graphics instance had before using this method.

	}
	
}
