import java.awt.Font;
import java.awt.Graphics;
import java.awt.Color;

/**
 * Objects of this type represent a speedOMeter.
 * @author Ivan J. Miranda Marrero
 *
 */
public class SpeedOMeter 
{
	private final int NUMBER_LABELS_SIZE; //Letter size of the labels.
	private final int NUMBER_OF_MARKS; //Number of speed marks.
	private final int ANGLE_BETWEEN_MARKS; //Angle between marks.
	private int xLocation; //SpeedOMeter's x location.
	private int yLocation; //SpeedOMeter's y location.
	private int radius;  //SpeedOMeter's radius.
	private int centerX; //SpeedOMeter's center x coordinate.
	private int centerY; //SpeedOMeter's center y coordinate.
	private double anglePerRevolution; //Angles per revolution. Used to indicate the pointer how much to change with each speed++.
	private int speed; //Speed in revolutions per minute.
	private final int MAX_SPEED; //The max speed that the SpeedOMeter can reach.
	
	/**
	 * Creates a car's SpeedOMeter (in RPM) with the properties given with each speed mark being a multiple of ten, starting at 0.
	 * @param xLocation the SpeedOMeter's x coordinate (top-left).
	 * @param yLocation the SpeedOMeter's y coordinate (top-left).
	 * @param radius the SpeedOMeter's x radius.
	 * @param NUMBER_OF_MARKS the SpeedOMeter's number of speed marks (4 multiple to guarantee function), including the one exactly at -90 degrees (or 270 degrees) although it will not be drawn.
	 * @param NUMBER_LABELS_SIZE the font size of the number labels for each mark.
	 */
	public SpeedOMeter(int xLocation, int yLocation, int radius, int NUMBER_OF_MARKS, int NUMBER_LABELS_SIZE)
	{
		this.xLocation = xLocation;
		this.yLocation = yLocation;
		this.radius = radius;
		this.NUMBER_OF_MARKS = NUMBER_OF_MARKS;
		this.NUMBER_LABELS_SIZE = NUMBER_LABELS_SIZE;
		ANGLE_BETWEEN_MARKS = (int)(360.0/NUMBER_OF_MARKS);
		centerX = xLocation + radius;
		centerY = yLocation + radius;
		speed = 0;
		MAX_SPEED = (NUMBER_OF_MARKS - 2)*10;
		anglePerRevolution = (360.0 - ANGLE_BETWEEN_MARKS*(NUMBER_OF_MARKS/4.0))/60; //Accounts for the part of the speedometer without marks.
	}
	
	/**
	 * The angle that one revolution change equals to in degrees. This then can be then used to synchronize the car gauge pointer to the current
	 * speedometer's speed, as it receives a position in degrees.
	 * @return the angle of one revolution change in degrees.
	 */
	public double getAnglePerRevolution()
	{
		return anglePerRevolution;
	}
	
	/**
	 * Returns the current speed that the SpeedOMeter has.
	 * @return the current speed in revolutions per minute.
	 */
	public int getSpeed()
	{
		return speed;
	}
	
	/**
	 * Returns the max speed that the SpeedOMeter can reach.
	 * @return the max speed in revolutions per minute.
	 */
	public int getMaxSpeed()
	{
		return MAX_SPEED;
	}
	/**
	 * Draws the current instance of this SpeedOMeter with the given properties.
	 * @param g the Graphics instance in which this object will be drawn.
	 * @param c the color in which it will be drawn.
	 * @param background the color of the background in which the object will be placed.
	 */
	public void draw(Graphics g, Color c, Color background)
	{
		Color originalColor = g.getColor(); //Saves original color in the Graphics instance.
		Font originalFont = g.getFont(); //Saves original font in the Graphics instance.
		
		g.setColor(c);
		for(int i = 0; i < NUMBER_OF_MARKS - 1; i++) //Draws speed marks starting at -45 degrees, ending at 225 degrees.
		{
			//Draws the corresponding arc for each mark.
			g.fillArc(xLocation, yLocation, radius*2, radius*2, (i-2)*ANGLE_BETWEEN_MARKS - 1, 2); 
		} 
		
		//Draws the border of the speed-o-meter and a white circle a little bit smaller, giving the illusion of speed marks.
		g.drawArc(xLocation, yLocation, radius*2, radius*2, 0, 360); 
		g.setColor(background);
		g.fillArc(xLocation + 8, yLocation + 8, radius*2 - 15, radius*2 - 15, 0, 360);
			
		//Draws the speed label for each mark. 7 labels in total, 7 iterations, not 8. Thus the "number of marks - 1" expression.		
		g.setFont(new Font("Monospaced", Font.BOLD, NUMBER_LABELS_SIZE));
		g.setColor(c);
		for (int i = 0; i < NUMBER_OF_MARKS - 1; i++)
		{   //Draws corresponding label in each iteration. Coordinates are obtained by pythagorean geometry.
			g.drawString( //x = circleCenterX + R*COS(z), y = circleCenterY + R*SIN(z), z = angle in RADIANS!!! (LOST ONE HOUR ON THAT! MY GOD, I SHOULD ALWAYS READ THE DOCUMENTATION BEFORE USING ANYTHING!)
						Integer.toString(0 + (i*10)), //String
						(int)((centerX - 10) + (radius - 20)*Math.cos(Math.toRadians((i+3)*ANGLE_BETWEEN_MARKS))), //X
						(int)((centerY + 7) + (radius - 20)*Math.sin(Math.toRadians((i+3)*ANGLE_BETWEEN_MARKS))) //Y
					    );
		} //End of iteration.
			
		//Draws RPM label.
		g.drawString( //RPM label.
					"RPM",
					(int)((centerX - 18) + (radius - 20)*Math.cos(Math.toRadians(2*ANGLE_BETWEEN_MARKS))),
					(int)((centerY - 45) + (radius - 20)*Math.sin(Math.toRadians(2*ANGLE_BETWEEN_MARKS)))
					);
		
		g.setColor(originalColor); //Sets back the color to the original color the Graphics instance had before using this method.
		g.setFont(originalFont); //Sets back the font to the original font the Graphics instance had before using this method.
	}
	
	/**
	 * Draws the speed controller with the current's speed.
	 * @param g the Graphics instance in which it will be drawn.
	 * @param c the color in which it will be drawn.
	 * @param indicatorColor the color of the indicator button.
	 * @param xLocation the xLocation of the controller.
	 * @param yLocation the yLocation of the controller.
	 * @param width the width of the controller.
	 * @param height the height of the controller.
	 */
	public void drawSpeedController(Graphics g, Color c, Color indicatorColor, double xLocation, double yLocation, double width, double height)
	{
		Color originalColor = g.getColor(); //Saves original color in the Graphics instance.
		
		double widthPerRevolution = width/(10*(NUMBER_OF_MARKS - 2)); //One width's revolution of 
		int numberOfDivisions = (10*(NUMBER_OF_MARKS - 2)); 
		double widthOfEachDivision = width/numberOfDivisions; //In order to move the controller accordingly with the actual speed.
		
		g.setColor(c);
		g.drawRect((int)xLocation, (int)yLocation, (int)width, (int)height); //Draws the outline.
		
		g.setColor(indicatorColor);
		g.fillRect((int)xLocation + 1, (int)(yLocation + height/4.0), (int)(speed*widthPerRevolution) - 1, (int)(height/2.0)); //Draws meter.
		
		g.setColor(c);
		for(int i = 1; i < numberOfDivisions; i++) //Draws line divisions whose size depends on what the division means. (e.g. one half, one eighth, etc.)
		{ 	
			if(i % (int)(numberOfDivisions/2.0) == 0) //for med speed.
				g.fillRect((int)(xLocation + i*widthOfEachDivision), (int)yLocation, 2, (int)height);
			else //for multiples of (1/numberOfDivisions) of the tank's width.
				g.fillRect((int)(xLocation + i*widthOfEachDivision), (int)(yLocation + (height/2.75)), 1, (int)(height - (height/1.5)));				
		}
		g.setColor(indicatorColor);
		g.fillRect((int)(xLocation + speed*widthPerRevolution - (2*widthPerRevolution)), (int)(yLocation + height/8.0), (int)(4*widthPerRevolution), (int)(3*height/4.0)); //Draws the outline.
		
		g.setColor(originalColor); //Sets back the color to the original color the Graphics instance had before using this method.
	}
	
	/**
	 * Increases speed by one revolution.
	 */
	public void increaseSpeed()
	{
		speed++;
	}
	
	/**
	 * Decreases speed by one revolution.
	 */
	public void decreaseSpeed()
	{
		speed--;
	}
	
	
}
