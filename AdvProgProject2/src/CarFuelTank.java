import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 * Objects of this type represent a car's fuel tank.
 * @author Ivan J. Miranda Marrero
 *
 */
public class CarFuelTank 
{
	private double xLocation; //The x coordinate of the fuel tank (top-left).
	private double yLocation; //The y coordinate of the fuel tank (top-left).
	private double width; //The width of the fuel tank.
	private double height; //The height of the fuel tank.
	private int tankCapacity; //The tank's capacity (in liters).
	private int actualLoad; //The tank's actual load (in liters).
	private double heightPerLiter; //One liter's height in the fuel tank.
	
	/**
	 * Creates a car's fuel tank with the given properties.
	 * @param xLocation the x coordinate of the fuel tank (top-left).
	 * @param yLocation the y coordinate of the fuel tank (top-left).
	 * @param width the width of the fuel tank.
	 * @param height the height of the fuel tank.
	 * @param tankCapacity the tank's capacity (in liters).
	 * @param actualLoad the tank's actual load (in liters).
	 */
	public CarFuelTank(double xLocation, double yLocation, double width, double height, int tankCapacity, int actualLoad)
	{
		this.xLocation = xLocation;
		this.yLocation = yLocation;
		this.width = width;
		this.height = height;
		this.tankCapacity = tankCapacity;
		this.actualLoad = actualLoad;
		heightPerLiter = height/this.tankCapacity; //int tankFractionMultiplier; actualHeight = tankFractionMultiplier*(height/tankCapacity);
	}
	
	/**
	 * Draws the current's instance of this fuel tank with the given properties.
	 * @param g the Graphics instance in which it will be drawn.
	 * @param c the color in which it will be drawn.
	 * @param gasColor the color of the gas.
	 */
	public void draw(Graphics g, Color c, Color gasColor)
	{
		Color originalColor = g.getColor(); //Saves original color in the Graphics instance.
		Font originalFont = g.getFont(); //Saves original font in the Graphics instance.
		
		g.setColor(c);
		g.drawRect((int)xLocation, (int)yLocation, (int)width, (int)height); //Draws outline.
		
		g.setColor(gasColor); //Draws actual load.
		g.fillRect((int)xLocation + 1, (int)(yLocation + (height - actualLoad*heightPerLiter)) + 1, (int)width - 1, (int)(actualLoad*heightPerLiter) - 1);
		
		g.setColor(originalColor); //Sets back the color to the original color the Graphics instance had before using this method.
		g.setFont(originalFont); //Sets back the font to the original font the Graphics instance had before using this method.
	}
	
	/**
	 * Draws a fuel meter corresponding to the fuel tank's actual load in the position given and with the properties given.
	 * @param g the Graphics instance in which it will be drawn.
	 * @param c the color in which it will be drawn.
	 * @param meterColor the color of the meter.
	 * @param xLocation the x coordinate of the meter.
	 * @param yLocation the y coordinate of the meter.
	 * @param width the width of the meter.
	 * @param height the height of the meter.
	 */
	public void drawFuelMeter(Graphics g, Color c, Color meterColor, double xLocation, double yLocation, double width, double height)
	{
		Color originalColor = g.getColor(); //Saves original color in the Graphics instance.
		
		double widthPerLiter = width/tankCapacity; //Same logic as heightPerLiter for the fuel tank.
		int numberOfDivisions = 32; 
		double widthOfEachDivision = width/numberOfDivisions; //In order to fill the meter accordingly with the actual load.
		
		g.setColor(c);
		g.drawRect((int)xLocation, (int)yLocation, (int)width, (int)height); //Draws the outline.
		
		g.setColor(meterColor);
		g.fillRect((int)xLocation + 1, (int)(yLocation + height/4.0), (int)(actualLoad*widthPerLiter) - 1, (int)(height/2.0)); //Draws meter.
		
		g.setColor(c);
		for(int i = 1; i < numberOfDivisions; i++) //Draws line divisions whose size depends on what the division means. (e.g. one half, one eighth, etc.)
		{ 	
			if(i % 8 == 0) //for multiples of (8/numberOfDivisions) of the tank's width.
				g.fillRect((int)(xLocation + i*widthOfEachDivision), (int)yLocation, 2, (int)height);
			else if(i % 4 == 0) //for multiples of (4/numberOfDivisions) of the tank's width.
				g.fillRect((int)(xLocation + i*widthOfEachDivision), (int)(yLocation + (height/4.0)), 1, (int)(height - (height/2.0)));
			else //for multiples of (1/numberOfDivisions) of the tank's width.
				g.fillRect((int)(xLocation + i*widthOfEachDivision), (int)(yLocation + (height/2.75)), 1, (int)(height - (height/1.5)));				
		}
		
		g.setColor(originalColor); //Sets back the color to the original color the Graphics instance had before using this method.
	}
}
